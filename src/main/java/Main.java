import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) throws IOException {
        String location = "Residence of hakier";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = new Date();
        String[] command = new String[]{"/home/pi/Desktop/Adafruit_Python_DHT/examples/AdafruitDHT.py", "22", "4"};

        Process proc = new ProcessBuilder(command).start();
        try {
            proc = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(proc.getInputStream()));

        FileWriter fileWriter = new FileWriter("/home/pi/Desktop/database.txt", true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter printWriter = new PrintWriter(bufferedWriter);

        String line = "";
        String temp = "";
        String hum = "";
        while ((line = reader.readLine()) != null) {
            System.out.print(line + "\n");
            StringTokenizer stringTokenizer = new StringTokenizer(line);
            while (stringTokenizer.hasMoreElements()) {
                temp = stringTokenizer.nextToken();
                hum = stringTokenizer.nextToken();
            }

            ArrayList list = new ArrayList();

            list.add(location);
            list.add(simpleDateFormat.format(date));
            list.add(temp);
            list.add(hum);

            String path = "/home/pi/Desktop/database.json";

            FileWriter fileWriter1 = new FileWriter(path, true);
            BufferedWriter bufferedWriter1 = new BufferedWriter(fileWriter1);
            PrintWriter printWriter1 = new PrintWriter(bufferedWriter1);

            printWriter.println(list);

            ObjectMapper mapper = new ObjectMapper();
//            mapper.writeValue(new File("/home/pi/Desktop/database.json"), list);
            printWriter1.println(mapper.writeValueAsString(list));
            printWriter1.close();


            ArrayList list3 = new ArrayList();
            list3.add("first line");
            list3.add("2nd line");
            ArrayList list2 = new ArrayList();
            list2.add(list3);
            list2.add(location);
            list2.add(simpleDateFormat.format(date));
            list2.add(temp);
            list2.add(hum);
            list2.add(list3);
            String path2 = "/home/pi/Desktop/database2.json";


            Writer writer = new FileWriter(path2, true);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(list2, writer);
            writer.close();

        }

        try {
            proc.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}