package components;

import java.text.SimpleDateFormat;

public class Date {
    java.util.Date date = new java.util.Date();
    //this is pattern to convert Date to specific format
    public String datePattern = "dd/MM/yyyy HH:mm";
    private String outputDate = "";

    public Date() {
    }

    public String date() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        //formatting actual date with datePattern
        outputDate = simpleDateFormat.format(date);
        //returns fixed date
        return outputDate;
    }
}
