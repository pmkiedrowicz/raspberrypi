package components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DHT22Reader {
    public static String[] command = new String[]{"/home/pi/Desktop/Adafruit_Python_DHT/examples/AdafruitDHT.py", "22", "4"};
    public String line = "";

    public String processTerminal() throws IOException {
        Process process = new ProcessBuilder(command).start();
        process = Runtime.getRuntime().exec(command);
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringTokenizer stringTokenizer=new StringTokenizer(line);
        while ((line = reader.readLine()) != null) {
            while (stringTokenizer.hasMoreElements()){
                line=stringTokenizer.nextToken();
            }
        }
        process.destroy();
        return line;
    }
}
