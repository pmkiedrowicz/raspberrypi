package components;

import java.io.IOException;

public class StringTokenizer {
    DHT22Reader dht22Reader = new DHT22Reader();
    String inputString = dht22Reader.processTerminal();
    java.util.StringTokenizer stringTokenizer = new java.util.StringTokenizer(inputString);
    String temperature = "";
    String humidity = "";

    public StringTokenizer() throws IOException {
        while (stringTokenizer.hasMoreElements()) {
            temperature = stringTokenizer.nextToken().replaceAll("[a-zA-Z=]", "");
            humidity = stringTokenizer.nextToken().replaceAll("[a-zA-Z=]", "");
        }
    }

    public String temperatureOutput() {
        return temperature;
    }

    public String humidityOutput() {
        return humidity;
    }
}
